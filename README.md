# Information / Информация

Интеграция информационных сообщений в статью.

## Install / Установка

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_Issue`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_Issue' );
```

## Syntax / Синтаксис

```html
{{#issue: [TYPE-1]|[TYPE-2]|[TYPE-3]}}
```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
